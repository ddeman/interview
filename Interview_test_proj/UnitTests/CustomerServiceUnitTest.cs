﻿using Interview_test_proj;
using Interview_test_proj.Data.Entities;
using Interview_test_proj.Infrastructure.Abstract;
using Interview_test_proj.Infrastructure.Concrete.Services;
using Interview_test_proj.Models.Customer;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace UnitTests
{
    [TestClass]
    public class CustomerServiceUnitTest
    {
        [TestMethod]
        public async Task GetAsync_ShouldReturnCustomerWithSameId()
        {
            // Arrange
            var customerSetup = new Customer() { Id = 2 };

            var mapper = AutoMapperConfig.CreateConfiguration().CreateMapper();

            var customerRepository = new Mock<IRepository<Customer>>();
            customerRepository
                .Setup(x => x.GetSingleExpandableAsync(It.Is<Expression<Func<Customer, bool>>>(y => y.Compile()(customerSetup)),
                    It.IsAny<Expression<Func<Customer, object>>>()))
                .Returns(Task.FromResult(customerSetup));

            // Act
            var customerService = new CustomerService(mapper, customerRepository.Object);
            var customerResult = await customerService
                .GetAsync(new SearchCustomerModel { CustomerId = customerSetup.Id });

            // Assert
            Assert.AreEqual(customerSetup.Id, customerResult.Id);
        }

        [TestMethod]
        public async Task GetAsync_ShouldReturnCustomerWithSameEmail()
        {
            // Arrange
            var customerSetup = new Customer() { Email = "test@unit.ex" };

            var mapper = AutoMapperConfig.CreateConfiguration().CreateMapper();

            var customerRepository = new Mock<IRepository<Customer>>();
            customerRepository
                .Setup(x => x.GetSingleExpandableAsync(It.Is<Expression<Func<Customer, bool>>>(y => y.Compile()(customerSetup)),
                    It.IsAny<Expression<Func<Customer, object>>>()))
                .Returns(Task.FromResult(customerSetup));

            // Act
            var customerService = new CustomerService(mapper, customerRepository.Object);
            var customerResult = await customerService
                .GetAsync(new SearchCustomerModel { Email = customerSetup.Email });

            // Assert
            Assert.AreEqual(customerSetup.Email, customerResult.Email);
        }

        [TestMethod]
        public async Task GetAsync_ShouldReturnCustomerWithSameIdAndEmail()
        {
            // Arrange
            var customerSetup = new Customer() { Id = 2, Email = "test@unit.ex" };

            var mapper = AutoMapperConfig.CreateConfiguration().CreateMapper();

            var customerRepository = new Mock<IRepository<Customer>>();
            customerRepository
                .Setup(x => x.GetSingleExpandableAsync(It.Is<Expression<Func<Customer, bool>>>(y => y.Compile()(customerSetup)),
                    It.IsAny<Expression<Func<Customer, object>>>()))
                .Returns(Task.FromResult(customerSetup));

            // Act
            var customerService = new CustomerService(mapper, customerRepository.Object);
            var customerResult = await customerService
                .GetAsync(new SearchCustomerModel { CustomerId = customerSetup.Id, Email = customerSetup.Email });

            // Assert
            Assert.AreEqual(customerSetup.Id, customerResult.Id);
            Assert.AreEqual(customerSetup.Email, customerResult.Email);
        }

        [TestMethod]
        public async Task GetAsync_ShouldReturnCustomerNull()
        {
            // Arrange
            var customerSetup = new Customer() { Id = 2, Email = "test@unit.ex" };
            var differentCustomerEmail = "unit@test.ex";

            var mapper = AutoMapperConfig.CreateConfiguration().CreateMapper();

            var customerRepository = new Mock<IRepository<Customer>>();
            customerRepository
                .Setup(x => x.GetSingleExpandableAsync(It.Is<Expression<Func<Customer, bool>>>(y => y.Compile()(customerSetup)),
                    It.IsAny<Expression<Func<Customer, object>>>()))
                .Returns(Task.FromResult(customerSetup));

            // Act
            var customerService = new CustomerService(mapper, customerRepository.Object);
            var customerResult = await customerService
                .GetAsync(new SearchCustomerModel { CustomerId = customerSetup.Id, Email = differentCustomerEmail });

            // Assert
            Assert.IsNull(customerResult);
        }
    }
}
