﻿using FluentValidation;
using Interview_test_proj.ViewModels.Customer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Interview_test_proj.Configurations.FluentValidator
{
    public class SearchCustomerViewModelValidator : AbstractValidator<SearchCustomerViewModel>
    {
        public SearchCustomerViewModelValidator()
        {
            RuleFor(searchModel => searchModel).Custom((searchModel, context) =>
            {
                if (!searchModel.CustomerId.HasValue && searchModel.Email == null)
                {
                    context.AddFailure("No inquiry criteria");
                }
            });
            RuleFor(searchModel => searchModel.CustomerId).GreaterThan(0).WithMessage("Invalid Customer Id");
            RuleFor(searchModel => searchModel.Email).EmailAddress().WithMessage("Invalid Email");
            RuleFor(searchModel => searchModel.Email).MaximumLength(25).WithMessage("Invalid Email");
        }
    }
}