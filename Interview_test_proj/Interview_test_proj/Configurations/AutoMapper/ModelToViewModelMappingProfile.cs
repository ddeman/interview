﻿using AutoMapper;
using Interview_test_proj.Extensions;
using Interview_test_proj.Models.Customer;
using Interview_test_proj.Models.Transaction;
using Interview_test_proj.ViewModels.Customer;
using Interview_test_proj.ViewModels.Transaction;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;

namespace Interview_test_proj.Configurations.AutoMapper
{
    public class ModelToViewModelMappingProfile : Profile
    {
        public new string ProfileName = "ModelToViewModelMappingProfile";

        public ModelToViewModelMappingProfile()
        {
            CreateMap<TransactionModel, TransactionViewModel>()
                .ForMember(dst => dst.Date, opts => opts.MapFrom(src => src.CreationDate.ToString("dd/MM/yyyy hh:mm", CultureInfo.InvariantCulture)))
                .ForMember(dst => dst.Currency, opts => opts.MapFrom(src => src.CurrencyCode))
                .ForMember(dst => dst.Status, opts => opts.MapFrom(src => src.Status.GetDescription()));

            CreateMap<CustomerModel, CustomerViewModel>()
                .ForMember(dst => dst.CusomerId, opts => opts.MapFrom(src => src.Id))
                .ForMember(dst => dst.Mobile, opts => opts.MapFrom(src => src.MobileNo))
                .ForMember(dst => dst.Transactions, opts =>
                {
                    opts.PreCondition(src => src.Transactions?.Any() ?? false);
                    opts.MapFrom(src => src.Transactions.OrderByDescending(x => x.CreationDate).Take(5));
                })
                .AfterMap((src, dest) => dest.Transactions = dest.Transactions ?? new List<TransactionViewModel>());
        }
    }
}