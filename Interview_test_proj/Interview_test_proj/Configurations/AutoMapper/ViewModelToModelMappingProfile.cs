﻿using AutoMapper;
using Interview_test_proj.Models.Customer;
using Interview_test_proj.ViewModels.Customer;

namespace Interview_test_proj.Configurations.AutoMapper
{
    public class ViewModelToModelMappingProfile : Profile
    {
        public new string ProfileName = "ViewModelToModelMappingProfile";

        public ViewModelToModelMappingProfile()
        {
            CreateMap<SearchCustomerViewModel, SearchCustomerModel>();
        }
    }
}