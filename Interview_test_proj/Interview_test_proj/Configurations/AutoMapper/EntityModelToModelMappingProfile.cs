﻿using AutoMapper;
using Interview_test_proj.Data.Entities;
using Interview_test_proj.Models.Customer;
using Interview_test_proj.Models.Transaction;

namespace Interview_test_proj.Configurations.AutoMapper
{
    public class EntityModelToModelMappingProfile : Profile
    {
        public new string ProfileName = "EntityModelToModelMappingProfile";

        public EntityModelToModelMappingProfile()
        {
            CreateMap<Transaction, TransactionModel>();
            CreateMap<Customer, CustomerModel>();
        }
    }
}