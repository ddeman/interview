﻿using System.ComponentModel;

namespace Interview_test_proj.Enums
{
    public enum TransactionStatus
    {
        [Description("Success")]
        Success,
        [Description("Failed")]
        Failed,
        [Description("Canceled")]
        Canceled
    }
}