﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Interview_test_proj.Models.Customer
{
    public class SearchCustomerModel
    {
        public int? CustomerId { get; set; }
        public string Email { get; set; }
    }
}