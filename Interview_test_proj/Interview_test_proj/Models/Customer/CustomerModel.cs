﻿using Interview_test_proj.Models.Transaction;
using System;
using System.Collections.Generic;

namespace Interview_test_proj.Models.Customer
{
    public class CustomerModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }
        public List<TransactionModel> Transactions { get; set; }
    }
}