﻿using Interview_test_proj.Enums;
using System;

namespace Interview_test_proj.ViewModels.Transaction
{
    public class TransactionViewModel
    {
        public int Id { get; set; }
        public string Date { get; set; }
        public decimal Amount { get; set; }
        public string Currency { get; set; }
        public string Status { get; set; }
    }
}