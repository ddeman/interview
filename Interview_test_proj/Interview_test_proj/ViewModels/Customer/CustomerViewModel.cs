﻿using Interview_test_proj.Data.Entities;
using Interview_test_proj.ViewModels.Transaction;
using System.Collections.Generic;

namespace Interview_test_proj.ViewModels.Customer
{
    public class CustomerViewModel
    {
        public int CusomerId { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }

        public List<TransactionViewModel> Transactions { get; set; }
    }
}