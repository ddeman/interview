﻿namespace Interview_test_proj.ViewModels.Customer
{
    public class SearchCustomerViewModel
    {
        public int? CustomerId { get; set; }
        public string Email { get; set; }
    }
}