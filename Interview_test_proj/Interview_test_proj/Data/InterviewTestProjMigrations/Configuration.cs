namespace Interview_test_proj.Data.InterviewTestProjMigrations
{
    using Interview_test_proj.Data.Entities;
    using Interview_test_proj.Enums;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Interview_test_proj.Data.InterviewTestProjDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
            MigrationsDirectory = @"Data\InterviewTestProjMigrations";
        }

        protected override void Seed(Interview_test_proj.Data.InterviewTestProjDbContext context)
        {
            var customersSeed = new Customer[] {
                new Customer { Id = 1, Email = "sin@city.com", Name = "Bruce Willis" },
                new Customer { Id = 2, Email = "john@wick.com", Name = "Keanu Reeves" },
                new Customer { Id = 3, Email = "departed@the.com", Name = "Leo DiCaprio" },
                new Customer { Id = 4, Email = "misarables@les.com", Name = "Hugh Jackmen" }
            };

            var transactionsSeed = new Transaction[] {
                new Transaction { CreationDate = DateTime.Now,  Amount = 4, CurrencyCode = "USD", Status = Enums.TransactionStatus.Success, CustomerId = 2 },
                new Transaction { CreationDate = DateTime.Now.AddHours(1),  Amount = 1, CurrencyCode = "USD", Status = Enums.TransactionStatus.Success, CustomerId = 3 },
                new Transaction { CreationDate = DateTime.Now.AddHours(2),  Amount = 4, CurrencyCode = "USD", Status = Enums.TransactionStatus.Success, CustomerId = 3 },
                new Transaction { CreationDate = DateTime.Now.AddHours(3),  Amount = 1, CurrencyCode = "USD", Status = Enums.TransactionStatus.Success, CustomerId = 4 },
                new Transaction { CreationDate = DateTime.Now.AddHours(4),  Amount = 1, CurrencyCode = "USD", Status = Enums.TransactionStatus.Success, CustomerId = 4 },
                new Transaction { CreationDate = DateTime.Now.AddHours(5),  Amount = 3, CurrencyCode = "USD", Status = Enums.TransactionStatus.Success, CustomerId = 4 },
                new Transaction { CreationDate = DateTime.Now.AddHours(6),  Amount = 7, CurrencyCode = "USD", Status = Enums.TransactionStatus.Success, CustomerId = 4 },
                new Transaction { CreationDate = DateTime.Now.AddHours(7),  Amount = 1, CurrencyCode = "USD", Status = Enums.TransactionStatus.Success, CustomerId = 4 },
                new Transaction { CreationDate = DateTime.Now.AddHours(8),  Amount = 5, CurrencyCode = "USD", Status = Enums.TransactionStatus.Success, CustomerId = 4 },
            };

            context.Customer.AddOrUpdate(x => x.Id, customersSeed);
            context.Transaction.AddOrUpdate(x => x.Id, transactionsSeed);
        }

        private TransactionStatus TransactionStatus(int v)
        {
            throw new NotImplementedException();
        }
    }
}
