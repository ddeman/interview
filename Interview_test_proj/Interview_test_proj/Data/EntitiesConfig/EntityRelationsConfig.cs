﻿using Interview_test_proj.Data.Entities;
using System.Data.Entity;

namespace Interview_test_proj.Data.EntitiesConfig
{
    public class EntityRelationsConfig
    {
        public static void Configure(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                .HasMany(c => c.Transactions)
                .WithRequired(tr => tr.Customer)
                .HasForeignKey(tr => tr.CustomerId);
        }
    }
}