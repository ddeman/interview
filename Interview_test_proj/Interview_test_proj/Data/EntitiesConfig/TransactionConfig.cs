﻿using Interview_test_proj.Data.Entities;
using System.Data.Entity;

namespace Interview_test_proj.Data.EntitiesConfig
{
    public class TransactionConfig
    {
        public static void Configure(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Transaction>()
                .HasKey(tr => tr.Id);

            modelBuilder.Entity<Transaction>()
                .Property(tr => tr.CreationDate)
                .IsRequired();

            modelBuilder.Entity<Transaction>()
                .Property(tr => tr.Amount)
                .IsRequired();

            modelBuilder.Entity<Transaction>()
                .Property(tr => tr.CurrencyCode)
                .HasMaxLength(5)
                .IsRequired();

            modelBuilder.Entity<Transaction>()
                .Property(tr => tr.Status)
                .IsRequired();
        }
    }
}