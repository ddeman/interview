﻿using Interview_test_proj.Data.Entities;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Annotations;

namespace Interview_test_proj.Data.EntitiesConfig
{
    public class CustomerConfig
    {
        public static void Configure(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Customer>()
                .HasKey(c => c.Id);

            modelBuilder.Entity<Customer>()
                .Property(c => c.Name)
                .HasMaxLength(30)
                .IsRequired();

            modelBuilder.Entity<Customer>()
                .Property(c => c.Email)
                .HasMaxLength(25)
                .IsRequired()
                .HasColumnAnnotation(
                    IndexAnnotation.AnnotationName,
                    new IndexAnnotation(new IndexAttribute("IX_CustomerEmail") { IsUnique = true })
                );

            modelBuilder.Entity<Customer>()
                .Property(c => c.MobileNo)
                .HasMaxLength(10);
        }
    }
}