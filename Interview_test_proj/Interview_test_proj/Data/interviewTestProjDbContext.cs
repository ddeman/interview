﻿using Interview_test_proj.Data.Entities;
using Interview_test_proj.Data.EntitiesConfig;
using System.Data.Entity;

namespace Interview_test_proj.Data
{
    public class InterviewTestProjDbContext : DbContext
    {
        public InterviewTestProjDbContext() : base("InterviewTestProj") { }

        public DbSet<Customer> Customer { get; set; }
        public DbSet<Transaction> Transaction { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            CustomerConfig.Configure(modelBuilder);
            TransactionConfig.Configure(modelBuilder);

            EntityRelationsConfig.Configure(modelBuilder);

            base.OnModelCreating(modelBuilder);
        }
    }
}