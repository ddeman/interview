﻿using Interview_test_proj.Enums;
using System;

namespace Interview_test_proj.Data.Entities
{
    public class Transaction
    {
        public int Id { get; set; }
        public DateTime CreationDate { get; set; }
        public decimal Amount { get; set; }
        public string CurrencyCode { get; set; }
        public TransactionStatus Status { get; set; }


        public int CustomerId { get; set; }
        public Customer Customer { get; set; }
    }
}