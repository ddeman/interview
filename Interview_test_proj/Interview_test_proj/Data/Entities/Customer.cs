﻿using System.Collections.Generic;

namespace Interview_test_proj.Data.Entities
{
    public class Customer
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string MobileNo { get; set; }

        public ICollection<Transaction> Transactions { get; set; }
    }
}