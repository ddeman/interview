﻿using Newtonsoft.Json;
using System.Net.Http.Formatting;
using System.Web.Http;

namespace Interview_test_proj
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();
            RouteConfig.Register(config.Routes);

            config.Formatters.Clear();
            config.Formatters.Add(
                new JsonMediaTypeFormatter()
                {
                    SerializerSettings = new JsonSerializerSettings()
                    {
                        Formatting = Formatting.Indented,
                        ReferenceLoopHandling = ReferenceLoopHandling.Ignore
                    }
                });
        }
    }
}
