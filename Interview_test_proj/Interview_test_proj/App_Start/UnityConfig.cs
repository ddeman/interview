using AutoMapper;
using FluentValidation;
using Interview_test_proj.Configurations.FluentValidator;
using Interview_test_proj.Data;
using Interview_test_proj.Infrastructure.Abstract;
using Interview_test_proj.Infrastructure.Concrete;
using Interview_test_proj.Infrastructure.Concrete.Services;
using Interview_test_proj.ViewModels.Customer;
using System;

using Unity;
using Unity.Lifetime;

namespace Interview_test_proj
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            container.RegisterInstance(typeof(IMapper), new Mapper(AutoMapperConfig.CreateConfiguration()), new SingletonLifetimeManager());

            container.RegisterType<IValidatorFactory, UnityValidatorFactory>(new ContainerControlledLifetimeManager());
            container.RegisterType<IValidator<SearchCustomerViewModel>, SearchCustomerViewModelValidator>(new ContainerControlledLifetimeManager());

            container.RegisterType<InterviewTestProjDbContext, InterviewTestProjDbContext>(new PerThreadLifetimeManager());
            container.RegisterType(typeof(IRepository<>), typeof(Repository<>), new TransientLifetimeManager());

            container.RegisterType<ICustomerService, CustomerService>(new TransientLifetimeManager());
        }
    }
}