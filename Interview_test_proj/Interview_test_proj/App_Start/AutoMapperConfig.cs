﻿using AutoMapper;
using Interview_test_proj.Configurations.AutoMapper;

namespace Interview_test_proj
{
    public class AutoMapperConfig
    {
        public static MapperConfiguration CreateConfiguration()
        {
            var mapperConfiguration = new MapperConfiguration(cfg =>
            {
                cfg.AddMaps(typeof(EntityModelToModelMappingProfile));
            });

            return mapperConfiguration;
        }
    }
}