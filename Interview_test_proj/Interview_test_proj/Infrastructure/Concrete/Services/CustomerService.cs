﻿using AutoMapper;
using Interview_test_proj.Data.Entities;
using Interview_test_proj.Factories;
using Interview_test_proj.Infrastructure.Abstract;
using Interview_test_proj.Models.Customer;
using System.Threading.Tasks;

namespace Interview_test_proj.Infrastructure.Concrete.Services
{
    public class CustomerService : ICustomerService
    {
        readonly IMapper _mapper;
        readonly IRepository<Customer> _customerRepository;
        public CustomerService(
            IMapper mapper,
            IRepository<Customer> customerRepository)
        {
            _mapper = mapper;
            _customerRepository = customerRepository;
        }

        public async Task<CustomerModel> GetAsync(SearchCustomerModel searchModel)
        {
            var criteria = PredicateFactory.GetSearchCustomerPredicate(searchModel);

            var customer = await _customerRepository.GetSingleExpandableAsync(criteria, x => x.Transactions);

            return _mapper.Map<CustomerModel>(customer);
        }
    }
}