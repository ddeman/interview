﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Interview_test_proj.Infrastructure.Abstract;
using Interview_test_proj.Data;
using System.Data.Entity;
using LinqKit;

namespace Interview_test_proj.Infrastructure.Concrete
{
    /// <summary>
    /// Generic repository for InterviewTestProjDbContext
    /// DbContext can be Generic too
    /// AddRange methods are not provided yet
    /// </summary>
    /// <typeparam name="TEntity">Data Entity Type</typeparam>
    public class Repository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        private readonly InterviewTestProjDbContext _dbContext;
        private readonly DbSet<TEntity> _dbSet;

        public Repository(InterviewTestProjDbContext dbContext)
        {
            _dbContext = dbContext;
            _dbSet = dbContext.Set<TEntity>();
        }

        public TEntity Create(TEntity entity)
        {
            _dbSet.Add(entity);
            return entity;
        }

        public TEntity GetSingle(Expression<Func<TEntity, bool>> criteria, params Expression<Func<TEntity, object>>[] includes)
        {
            var query = _dbSet.Where(criteria);
            return includes.Aggregate(query, (current, includeProperty) => current.Include(includeProperty)).FirstOrDefault();
        }
        public async Task<TEntity> GetSingleAsync(Expression<Func<TEntity, bool>> criteria, params Expression<Func<TEntity, object>>[] includes)
        {
            var query = _dbSet.Where(criteria);
            return await includes.Aggregate(query, (current, includeProperty) => current.Include(includeProperty)).FirstOrDefaultAsync();
        }
        public async Task<TEntity> GetSingleExpandableAsync(Expression<Func<TEntity, bool>> criteria, params Expression<Func<TEntity, object>>[] includes)
        {
            var queryableResultWithIncludes = includes
                .Aggregate(_dbSet.AsQueryable(),
                    (current, include) => current.Include(include));
            return await queryableResultWithIncludes.AsExpandable().Where(criteria).FirstOrDefaultAsync();
        }

        public IEnumerable<TEntity> Get(Expression<Func<TEntity, bool>> criteria, params Expression<Func<TEntity, object>>[] includes)
        {
            var query = _dbSet.Where(criteria);
            return includes.Aggregate(query, (current, includeProperty) => current.Include(includeProperty)).ToList();
        }

        public async Task<IEnumerable<TEntity>> GetAsync(Expression<Func<TEntity, bool>> criteria, params Expression<Func<TEntity, object>>[] includes)
        {
            var query = _dbSet.Where(criteria);
            return await includes.Aggregate(query, (current, includeProperty) => current.Include(includeProperty)).ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> GetExpandableAsync(Expression<Func<TEntity, bool>> criteria, params Expression<Func<TEntity, object>>[] includes)
        {
            var queryableResultWithIncludes = includes
                .Aggregate(_dbSet.AsQueryable(),
                    (current, include) => current.Include(include));
            return await queryableResultWithIncludes.AsExpandable().Where(criteria).ToListAsync();
        }

        public TEntity Update(TEntity entity)
        {
            _dbContext.Entry(entity).State = EntityState.Modified;
            return entity;
        }

        public void Delete(TEntity entity)
        {
            _dbSet.Remove(entity);
        }

        public void SaveChanges()
        {
            // TODO: Implement UnitOfWork
            _dbContext.SaveChanges();
        }
    }
}
