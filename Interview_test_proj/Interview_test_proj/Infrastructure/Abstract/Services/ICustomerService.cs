﻿using Interview_test_proj.Data.Entities;
using Interview_test_proj.Models.Customer;
using Interview_test_proj.ViewModels.Customer;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Interview_test_proj.Infrastructure.Abstract
{
    public interface ICustomerService
    {
        Task<CustomerModel> GetAsync(SearchCustomerModel searchModel);
    }
}