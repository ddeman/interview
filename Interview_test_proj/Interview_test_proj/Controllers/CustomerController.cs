﻿using AutoMapper;
using FluentValidation;
using Interview_test_proj.Infrastructure.Abstract;
using Interview_test_proj.Models.Customer;
using Interview_test_proj.ViewModels.Customer;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace Interview_test_proj.Controllers
{
    public class CustomerController : ApiController
    {
        readonly IMapper _mapper;
        readonly IValidator<SearchCustomerViewModel> _validator;
        readonly ICustomerService _customerService;

        public CustomerController(
            IMapper mapper,
            IValidator<SearchCustomerViewModel> validator,
            ICustomerService customerService)
        {
            _mapper = mapper;
            _validator = validator;
            _customerService = customerService;
        }

        /// <summary>
        /// Get customer
        /// </summary>
        /// <remarks>
        /// Get customer by inquiry
        /// </remarks>
        [HttpPost]
        public async Task<IHttpActionResult> Get(SearchCustomerViewModel model)
        {
            try
            {
                var validationResult = _validator.Validate(model);

                if (!validationResult.IsValid)
                {
                    return BadRequest(validationResult.ToString(" "));
                }

                var customer = await _customerService.GetAsync(_mapper.Map<SearchCustomerModel>(model));

                if (customer == null)
                {
                    return NotFound();
                }

                return Ok(_mapper.Map<CustomerViewModel>(customer));
            }
            catch (Exception ex)
            {
                // TODO: provide exception handling and logging
                return BadRequest();
            }
        }
    }
}
