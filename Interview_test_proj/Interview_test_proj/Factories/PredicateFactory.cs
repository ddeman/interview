﻿using Interview_test_proj.Data.Entities;
using Interview_test_proj.Models.Customer;
using LinqKit;
using System;
using System.Linq.Expressions;

namespace Interview_test_proj.Factories
{
    public class PredicateFactory
    {
        public static Expression<Func<Customer, bool>> GetSearchCustomerPredicate(SearchCustomerModel searchModel)
        {
            var criteria = PredicateBuilder.New<Customer>(true);

            if (searchModel.CustomerId.HasValue)
            {
                criteria = criteria.And(x => x.Id == searchModel.CustomerId);
            }

            if (!string.IsNullOrEmpty(searchModel.Email))
            {
                criteria = criteria.And(x => string.Equals(x.Email, searchModel.Email));
            }

            return criteria;
        }
    }
}